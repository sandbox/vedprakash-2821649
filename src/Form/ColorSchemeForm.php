<?php

/**
 * @file
 * Contains Drupal\color_scheme\Form\ColorSchemeForm.
 */

namespace Drupal\color_scheme\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\color_scheme\DB\ColorScheme;

/**
 * Class ColorSchemeForm.
 *
 * @package Drupal\color_scheme\Form
 */
class ColorSchemeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'color_scheme_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $color_values = ColorScheme::load(\Drupal::currentUser()->id());

    $form['color_scheme'] = array(
      '#type' => 'vertical_tabs',
    );

    $form['background_color'] = array(
      '#type' => 'details',
      '#title' => $this->t('Background Color'),
      '#group' => 'color_scheme',
    );

    $form['background_color']['bg_hex_code'] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#default_value' => isset($color_values->background_color) ? $color_values->background_color : '',
      '#description' => '<div id="bg-hex-code"></div>'
    );

    $form['text_color'] = array(
      '#type' => 'details',
      '#title' => $this->t('Text Color'),
      '#group' => 'color_scheme',
    );

    $form['text_color']['selectors'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Selectors'),
      '#default_value' => isset($color_values->selectors) ? $color_values->selectors : '',
      '#attributes' => array('placeholder' => t('Enter the selectors seperated by comma.'))
    );

    $form['text_color']['text_hex_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Color'),
      '#default_value' => isset($color_values->text_color) ? $color_values->text_color : '',
      '#description' => '<div id="text-hex-code"></div>'
    );

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#name' => 'save',
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to Default'),
      '#name' => 'reset',
    ];
    $form['#attached']['library'][] = 'color_scheme/color_scheme.elements';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $button_clicked = $form_state->getTriggeringElement()['#name'];
    if ($button_clicked == 'save') {
      $values = $form_state->getValues();
      $entry = array();
      $entry['uid'] = \Drupal::currentUser()->id();
      $entry['background_color'] = $values['bg_hex_code'];
      $entry['selectors'] = $values['selectors'];
      $entry['text_color'] = $values['text_hex_code'];
      ColorScheme::operation($entry);
    }
    if ($button_clicked == 'reset') {
      $entry = array();
      $entry['uid'] = \Drupal::currentUser()->id();
      ColorScheme::delete($entry);
      drupal_set_message(t('Successfully reset the Color Scheme.'));
    }
  }

}
