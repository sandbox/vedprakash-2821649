<?php

/**
 * @file
 * Contains Database Operations.
 */

namespace Drupal\color_scheme\DB;

/**
 * Class ColorScheme.
 */
class ColorScheme {

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   */
  public static function operation($entry) {
    $color_values = ColorScheme::load($entry['uid']);
    if (isset($color_values->uid) && !empty($color_values->uid)) {
      $entry['updated'] = time();
      // Update Existing Record
      ColorScheme::update($entry);
      drupal_set_message(t('Successfully updated the Color Scheme.'));
    }
    else {
      $entry['created'] = $entry['updated'] = time();
      // Insert New Record
      ColorScheme::insert($entry);
      drupal_set_message(t('Successfully added the Color Scheme.'));
    }
  }

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of record inserted.
   *
   * @throws \Exception
   *   When the database insert fails.
   *
   * @see db_insert()
   */
  public static function insert($entry) {
    $return_value = NULL;
    try {
      $return_value = db_insert('color_scheme')
          ->fields($entry)
          ->execute();
    }
    catch (\Exception $e) {
      drupal_set_message(t('db_insert failed. Message = %message, query= %query', array(
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
              )
          ), 'error');
    }
    return $return_value;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of record updated.
   *
   * @see db_update()
   */
  public static function update($entry) {
    try {
      $count = db_update('color_scheme')
          ->fields($entry)
          ->condition('uid', $entry['uid'])
          ->execute();
    }
    catch (\Exception $e) {
      drupal_set_message(t('db_update failed. Message = %message, query= %query', array(
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
              )
          ), 'error');
    }
    return $count;
  }

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the person identifier 'uid' element of the
   *   entry to delete.
   *
   * @see db_delete()
   */
  public static function delete($entry) {
    db_delete('color_scheme')
        ->condition('uid', $entry['uid'])
        ->execute();
  }

  /**
   *
   * @param int
   *   Accept uid and fetch the record from Database.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see db_select()
   */
  public static function load($uid) {
    $select = db_select('color_scheme', 'cs');
    $select->fields('cs');
    $select->condition('cs.uid', $uid);

    // Return the result in object format.
    return $select->execute()->fetchObject();
  }

}
