/**
 * @file Contains the definition of the behaviour Color Scheme.
 */

(function ($) {
    "use strict";
    /**
     * Attaches the Color Scheme Behaviour.
     */
    Drupal.behaviors.color_scheme = {
        attach: function (context, settings) {

            jQuery('#bg-hex-code').farbtastic('#edit-bg-hex-code');
            jQuery('#text-hex-code').farbtastic('#edit-text-hex-code');

        }
    };
})(jQuery);
