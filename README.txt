SUMMARY - Color Scheme
========================

Color Scheme is a simple drupal module which allows user's to change the 
background color and text color of their choice.

Installation:
-------------

Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-8

Configuration:
--------------

Go to admin/people/permissions & assign permission 'configure color scheme'.

Usage:
------

User's need to visit user/config/color-scheme for color scheme configuration.

For Support:
--------

https://www.drupal.org/u/vedprakash
